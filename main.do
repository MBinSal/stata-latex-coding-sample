** DATA TASK ** 
** Applicant * 

// Set working directory, run directory.do before you run main.do:
do setup/directory.do 

// Data Cleaning: 
do data_cleaning/clean.do 

// Install if not present in your program: 
ssc install estout, replace 

//Analysis: 
do analysis_code/q1.do 
do analysis_code/q2.do 
do analysis_code/q3.do

* End of main file *