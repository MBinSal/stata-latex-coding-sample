*******************************************

				READ ME 

*******************************************

This data task is completed using STATA. 

* STEP 1: 
Set working directory in directory.do file which is in the setup folder. Then run this file first, otherwise main.do will not run. 

* STEP 2: 
You just need to run the main.do file to run the entire project. 

* STEP 3: (Optional)
The latex code for report.pdf is avaialble in tex folder. You can reproduce that code as well by running the report.tex file. 

**	End of readme file	** 
