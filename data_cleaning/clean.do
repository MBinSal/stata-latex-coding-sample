* This file cleans the available dataset * 

clear 
cls 

// Read in the data csv file : 
import delimited "${DATA_PATH}/RA_21_22.csv"
save "${DATA_PATH}/data.dta", replace 

// Cleaning variables for analysis: 
sort year 

*Female = 1 and Male = 2
encode sex, generate(gender) 
drop sex 

* College = 1 , no college = 2 and some college = 3
encode education, generate(college)
drop education 

* White = 4, Other = 3, Black = 2 and Hispanic = 1
encode race, generate(ethnicity)
drop race

gen net_asset = asset_total - debt_total 


* End of cleaning script * 

