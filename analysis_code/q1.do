// Question 1: 

clear 
cls 

do data_cleaning/clean.do 

egen wtmedian = median(net_asset)

levelsof ethnicity, local(eth)
levelsof college, local(coll)
qui foreach l of local eth {
	qui foreach c of local coll {
		summarize net_asset [w = weight] if ethnicity == `l' & college == `c', detail
		replace wtmedian = r(p50) if ethnicity == `l' & college == `c' 
	}
}

ssc install schemepack, replace 
set scheme white_tableau  

line wtmedian year if ethnicity == 1 || ///
line wtmedian year if ethnicity == 2 || ///
line wtmedian year if ethnicity == 3 || ///
line wtmedian year if ethnicity == 4, ///
 	by(college) ///
 	ylabel(, angle(h)) ///
 	ytitle(Median Net Wealth) ///
 	xtitle(Year) ///
 	legend(label(1 "Hispanic") ///
 		 	label(2 "Black") ///
 		  	label(3 "Other") ///
 		   	label(4 "White"))

graph export "out/q1.png", replace 

* End of question 1 script * 
