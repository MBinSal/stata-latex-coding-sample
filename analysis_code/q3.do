//Question 3: 

clear
cls

qui do analysis_code/q1.do 

gen house = 1 if asset_housing != 0 & asset_housing != . 

preserve 
tempfile q3

keep if age >= 25 & house == 1 

save "`q3'", replace 
restore 

use `q3', clear 
keep if ethnicity == 2 | ethnicity == 4 
*Housing wealth 
gen non_house_asset = (asset_total - asset_housing) - debt_total
gen house_wealth = asset_housing - debt_housing 

gen wtmedian_non_house = . 
gen wtmedian_house = . 

levelsof ethnicity, local(eth)
qui foreach l of local eth {
		summarize house_wealth [w = weight] if ethnicity == `l', detail
		replace wtmedian_house = r(p50) if ethnicity == `l' 
}
levelsof ethnicity, local(eth)
qui foreach l of local eth {
		summarize non_house_asset [w = weight] if ethnicity == `l', detail
		replace wtmedian_non_house = r(p50) if ethnicity == `l' 
}


line wtmedian_house year || ///
line wtmedian_non_house year, ///
	by(ethnicity) /// 
 	ylabel(, angle(h)) ///
 	ytitle(Median Wealth) ///
 	xtitle(Year) ///
 	legend(label(1 "Median Net Housing wealth") ///
 		 	label(2 "Median Net Non Housing Wealth")) 

graph export "out/q3_a.png", replace 


line wtmedian_house year, /// 
	by(ethnicity) /// 
	title(Housing wealth trend post 2007) /// 
	xline(2007, lpattern(dash) lcolor(red)) /// 
 	ylabel(, angle(h)) ///
 	ytitle(Total Housing Wealth) ///
 	xtitle(Year) 

graph export "out/q3_b.png", replace 

summarize house_wealth [w = weight] if ethnicity == 2 & year >= 2007, detail

est clear 

estpost tabstat house_wealth [w = weight] if ethnicity == 2 & year >= 2007, c(stat) stat(n mean median)
esttab using "out/tex_out/table1.tex", replace ///
 cells("count mean p50") nonumber ///
  nomtitle nonote noobs label collabels("Count" "Mean" "Median")

est clear 

estpost tabstat house_wealth [w = weight] if ethnicity == 4 & year >= 2007, c(stat) stat(n mean median)
esttab using "out/tex_out/table2.tex", replace ///
 cells("count mean p50") nonumber ///
  nomtitle nonote noobs label collabels("Count" "Mean" "Median")

est clear 

* End of question 3 script * 