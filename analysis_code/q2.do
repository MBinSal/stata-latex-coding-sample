// Question 2: 
clear
cls

qui do analysis_code/q1.do 


line wtmedian year if ethnicity == 2 & college == 1 || ///
line wtmedian year if ethnicity == 4 & college == 1, ///
	title(People with a college degree) ///
 	ylabel(, angle(h)) ///
 	ytitle(Median Net Wealth) ///
 	xtitle(Year) ///
 	legend(label(1 "Black") ///
 		   label(2 "White")) 

graph export "out/q2_a.png", replace 

line wtmedian year if ethnicity == 2 & college == 2 || ///
line wtmedian year if ethnicity == 4 & college == 2, ///
	title(People without any college education) ///
 	ylabel(, angle(h)) ///
 	ytitle(Median Net Wealth) ///
 	xtitle(Year) ///
 	legend(label(1 "Black") ///
 		   label(2 "White")) 
 		   
graph export "out/q2_b.png", replace 

line wtmedian year if ethnicity == 2 & college == 3 || ///
line wtmedian year if ethnicity == 4 & college == 3, ///
	title(People with some college education) ///
 	ylabel(, angle(h)) ///
 	ytitle(Median Net Wealth) ///
 	xtitle(Year) ///
 	legend(label(1 "Black") ///
 		   label(2 "White")) 
 		   
graph export "out/q2_c.png", replace 

line wtmedian year if ethnicity == 2 & college == 1 || ///
line wtmedian year if ethnicity == 2 & college == 2 || ///
line wtmedian year if ethnicity == 2 & college == 3, ///
	title(Black Ethnicity earnings growth) /// 
 	ylabel(, angle(h)) ///
 	ytitle(Median Net Wealth) ///
 	xtitle(Year) ///
 	legend(label(1 "college") ///
 		   label(2 "no college") ///
 		   label(3 "some college"))

graph export "out/q2_d.png", replace 

line wtmedian year if ethnicity == 4 & college == 1 || ///
line wtmedian year if ethnicity == 4 & college == 2 || ///
line wtmedian year if ethnicity == 4 & college == 3, ///
	title(White Ethnicity earnings growth) /// 
 	ylabel(, angle(h)) ///
 	ytitle(Median Net Wealth) ///
 	xtitle(Year) ///
 	legend(label(1 "college") ///
 		   label(2 "no college") ///
 		   label(3 "some college"))

graph export "out/q2_e.png", replace 

* End of question 2 script * 